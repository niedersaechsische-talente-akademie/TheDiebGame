package com.diebgame.nta;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TheDiebGame extends ApplicationAdapter {
    SpriteBatch batch;

    enum GameState {
        Menu, Settings, Ingame, GameOver
    }

    GameState gameState = GameState.Menu;

    //------------------------------------------------Menu Variables--------------------------------------------------
    Texture redcross, greencross, musicPng, zurueck, geraeschePng, charakterPng,
            steuerungLinksPng, backgroundMenuOpen, backgroundMenuClosed, backgroundSettings, SettingsIcon;
    Music music;
    Sound doorsound;
    Sound halloechen;
    Preferences prefs;
    boolean musicOn;
    boolean soundsOn;
    boolean character_icon;
    boolean leftHand;
    boolean soundplay;
    int maxHeight;
    BitmapFont font;

    //------------------------------------------------Game Variables--------------------------------------------------
    OrthographicCamera camera;
    Texture police_icon, gamepad, wallVertical, wallHorizontal, doorVertical, doorHorizontal,
            background, gameOverScreen, diebFrame1, diebFrame2, diebFrame3, diebinFrame1, diebinFrame2, diebinFrame3;
    TextureRegion player_icon;
    Animation walkMale, walkFemale;

    public static final int
            STARTX = 100,
            STARTY = 100,
            GAMEPAD_DISTANCE = 50,
            PLAYER_SPEED = 500,
            PLAYER_SIZE = 100,
            POLICE_SPEED = PLAYER_SPEED / 2,
    //POLICE_LIGHT_DISTANCE = 5,
    ITEM_SIZE = 50;

    int GAMEPAD_SIZE = 500,
            GAMEPAD_BUTTON_SIZE = GAMEPAD_SIZE / 3,
            currentLevel = 1;
    float time;
    Texture[] itemIcons;
    levels[] level;
    item[] items;
    police[] policemen;
    player Player;

    class levels {
        int wallCountX, wallCountY, policemen_count, item_count;
        int[][] wallVertical, wallHorizontal;
        int wallLength, wallThickness;

        levels(int x, int y, int[][] horizontal, int[][] vertical, int Policemen_count, int Item_count, int l, int t) {
            this.wallCountX = x;
            this.wallCountY = y;
            this.wallHorizontal = new int[wallCountX][wallCountY];
            this.wallHorizontal = horizontal;
            this.wallVertical = new int[wallCountX][wallCountY];
            this.wallVertical = vertical;
            policemen_count = Policemen_count;
            item_count = Item_count;
            wallLength = l;
            wallThickness = t;
        }
    }

    class character {
        float x, y;
        int dir, ms;
    }

    class police extends character {
        //float last_turn, turnSpeed;
        police(float X, float Y) {
            x = X;
            y = Y;
            dir = 4;
            ms = POLICE_SPEED;
            //turnSpeed = 500;		//ms
            //last_turn = 0;
        }
    }

    class player extends character {
        int score;

        player() {
            x = STARTX;
            y = STARTY;
            dir = 4;
            ms = PLAYER_SPEED;
            score = 0;
        }
    }

    class item extends character {
        boolean colected;
        Texture icon;

        item(int X, int Y, Texture Icon) {
            x = X;
            y = Y;
            icon = Icon;
            colected = false;
        }
    }

    //---------------------------------------Maps---------------------------------------------------------
    public void createLevel() {
        level = new levels[3];
        //new level({x*{...}}, {{y*?}, ...}, wallHorizontal, wallVertical, policemen_count, item_count, .wallLength, wallThickness);

        level[0] = new levels(4, 9,
                new int[][]{{1, 1, 0, 1, 1, 0, 0, 1, 1}, {1, 0, 0, 0, 1, 0, 2, 0, 1}, {1, 0, 1, 0, 0, 0, 1, 2, 1}, {1, 0, 0, 0, 0, 1, 0, 1, 1}},
                new int[][]{{1, 1, 1, 1, 1, 1, 1, 1, 1}, {0, 2, 1, 0, 0, 0, 0, 1, 1}, {0, 0, 0, 0, 1, 0, 2, 1, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1}},
                3, 7, 150, 20);

        level[1] = new levels(6, 9,
                new int[][]{{1, 1, 0, 0, 1, 1, 0, 1, 1}, {1, 0, 1, 0, 0, 0, 1, 2, 1}, {1, 1, 0, 0, 1, 0, 1, 0, 1}, {1, 0, 0, 0, 0, 1, 0, 2, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1}},
                new int[][]{{1, 1, 1, 1, 1, 1, 1, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 1, 0, 1, 0, 1, 2, 1, 1}, {1, 1, 1, 0, 0, 1, 0, 1, 1}, {1, 1, 1, 0, 1, 0, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1}},
                5, 10, 250, 10);

        level[2] = new levels(3, 4, new int[][]{{1, 0, 0, 1}, {1, 0, 0, 2}, {0, 0, 0, 0}}, new int[][]{{1, 1, 1, 0}, {0, 0, 0, 0}, {1, 1, 1, 0}}, 2, 5, 200, 10);
    }

    //---------------------------------------Menu Functions-----------------------------------------------------
    public void initialiseMenu() {
        music = Gdx.audio.newMusic(Gdx.files.internal("music.mp3"));
        doorsound = Gdx.audio.newSound(Gdx.files.internal("doorsound.mp3"));
        halloechen = Gdx.audio.newSound(Gdx.files.internal("Halloechen.m4a"));
        redcross = new Texture("redcross.png");
        greencross = new Texture("greencross.png");
        musicPng = new Texture("Music.png");
        geraeschePng = new Texture("Geraeusche.png");
        charakterPng = new Texture("Charakter.png");
        steuerungLinksPng = new Texture("SteuerungLinks.png");
        zurueck = new Texture("Zurueck.png");
        prefs = Gdx.app.getPreferences("settings");
        maxHeight = Gdx.graphics.getHeight();
        backgroundMenuOpen = new Texture("BackgroundMenuOpen.png");
        backgroundMenuClosed = new Texture("BackgroundMenuClosed.png");
        backgroundSettings = new Texture("BackgroundSettings.png");
        SettingsIcon = new Texture("zahnrad.png");
        gameOverScreen = new Texture("fabi.png");
        Preferences prefs = Gdx.app.getPreferences("settings");
        musicOn = prefs.getBoolean("MusicOn", true);
        soundsOn = prefs.getBoolean("SoundsOn", true);
        leftHand = prefs.getBoolean("LeftHand", false);
        character_icon = prefs.getBoolean("Character", true);
        font = new BitmapFont();
        font.getData().setScale(scale(9, true), scale(9, false));
        soundplay = false;
        if (musicOn) {
            music.setLooping(true);
            music.play();
        } else {
            music.stop();

            maxHeight = Gdx.graphics.getHeight();
        }
    }

    public int scale(int position, boolean direction) {
        if (direction) {
            return position * Gdx.graphics.getWidth() / 1920;    //width
        } else {
            return position * Gdx.graphics.getHeight() / 1080;    //height
        }
    }

    public boolean settingsClick(int x, int y) {
        return (Gdx.input.getX() > x && Gdx.input.getX() < x + scale(1300, true) && Gdx.input.getY() > y && Gdx.input.getY() < y + scale(100, false));
    }

    public boolean settingsClickSmall(int x, int y) {
        return (Gdx.input.getX() > x && Gdx.input.getX() < x + scale(350, true) && Gdx.input.getY() > y && Gdx.input.getY() < y + scale(100, false));
    }

    public void menu() {
        batch.draw(new TextureRegion(backgroundMenuClosed), 0, 0, scale(1920, true), scale(1080, false));
        batch.draw(new TextureRegion(SettingsIcon), scale(1720, true), scale(880, false), scale(150, true), scale(150, false));
        if (Gdx.input.getX() > scale(660, true) && Gdx.input.getX() < scale(1260, false) && Gdx.input.getY() > scale(100, true) && Gdx.input.getY() < scale(980, false)) {
            if (soundsOn) {
                if (!soundplay) {
                    doorsound.play();
                    soundplay = true;
                }
            }

            batch.draw(new TextureRegion(backgroundMenuOpen), 0, 0, scale(1920, true), scale(1080, false));
            batch.draw(new TextureRegion(SettingsIcon), scale(1720, true), scale(880, false), scale(150, true), scale(150, false));
            if (!Gdx.input.isTouched()) {
                gameState = GameState.Ingame;
                setGameCamera();
            }
        }
        if (Gdx.input.justTouched()) {
            if (settingsClickSmall(scale(1720, true), scale(100, false))) {
                gameState = GameState.Settings;
            }
        }

        if (!Gdx.input.isTouched()) {
            soundplay = false;
        }
    }

    public void settings() {
        musicOn = prefs.getBoolean("MusicOn", true);
        soundsOn = prefs.getBoolean("SoundsOn", true);
        leftHand = prefs.getBoolean("LeftHand", false);
        character_icon = prefs.getBoolean("Character", true);
        batch.draw(new TextureRegion(backgroundSettings), 0, 0, scale(1920, true), scale(1080, false));
        if (Gdx.input.justTouched()) {
            if (settingsClick(scale(100, true), scale(100, false))) {
                if (musicOn) {
                    prefs.putBoolean("MusicOn", false);
                } else {
                    prefs.putBoolean("MusicOn", true);
                }
            } else if (settingsClick(scale(100, true), scale(300, false))) {
                if (soundsOn) {
                    prefs.putBoolean("SoundsOn", false);
                } else {
                    prefs.putBoolean("SoundsOn", true);
                }
            } else if (settingsClick(scale(100, true), scale(500, false))) {
                if (leftHand) {
                    prefs.putBoolean("LeftHand", false);
                } else {
                    prefs.putBoolean("LeftHand", true);
                }
            } else if (settingsClickSmall(scale(1000, true), scale(750, false))) {
                if (!character_icon) {
                    prefs.putBoolean("Character", true);
                }
            } else if (settingsClickSmall(scale(1500, true), scale(750, false))) {
                if (character_icon) {
                    prefs.putBoolean("Character", false);
                }
            } else if (settingsClick(scale(1720, true), scale(100, false))) {
                gameState = GameState.Menu;
            }
        }
        prefs.flush();
        if (musicOn) {
            music.setLooping(true);
            music.play();
        } else {
            music.stop();
        }
        batch.draw(new TextureRegion(zurueck), scale(1720, true), scale(880, false), scale(150, true), scale(150, false));
        batch.draw(new TextureRegion(musicPng), scale(250, true), scale(850, false), scale(1200, true), scale(200, false));
        batch.draw(new TextureRegion(geraeschePng), scale(250, true), scale(650, false), scale(1200, true), scale(200, false));
        batch.draw(new TextureRegion(steuerungLinksPng), scale(250, true), scale(450, false), scale(1200, true), scale(200, false));
        batch.draw(new TextureRegion(charakterPng), scale(250, true), scale(200, false), scale(1200, true), scale(200, false));
        batch.draw(new TextureRegion(diebFrame2), scale(1150, true), scale(540 - 360, false), scale(200, true), scale(200, false));
        batch.draw(new TextureRegion(diebinFrame2), scale(1650, true), scale(540 - 360, false), scale(200, true), scale(200, false));
        if (musicOn) {
            batch.draw(new TextureRegion(greencross), scale(100, true), scale(880, false), scale(100, true), scale(100, false));
        } else {
            batch.draw(new TextureRegion(redcross), scale(100, true), scale(1240 - 360, false), scale(100, true), scale(100, false));
        }
        if (soundsOn) {
            batch.draw(greencross, scale(100, true), scale(1040 - 360, false), scale(100, true), scale(100, false));
        } else {
            batch.draw(redcross, scale(100, true), scale(1040 - 360, false), scale(100, true), scale(100, false));
        }
        if (leftHand) {
            batch.draw(greencross, scale(100, true), scale(840 - 360, false), scale(100, true), scale(100, false));
        } else {
            batch.draw(redcross, scale(100, true), scale(840 - 360, false), scale(100, true), scale(100, false));
        }
        if (character_icon) {
            batch.draw(greencross, scale(1000, true), scale(590 - 360, false), scale(100, true), scale(100, false));
            batch.draw(redcross, scale(1500, true), scale(590 - 360, false), scale(100, true), scale(100, false));
        } else {
            batch.draw(redcross, scale(1000, true), scale(590 - 360, false), scale(100, true), scale(100, false));
            batch.draw(greencross, scale(1500, true), scale(590 - 360, false), scale(100, true), scale(100, false));
        }
    }

    //---------------------------------------Game Functions-----------------------------------------------------
    public void initializeGame() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        police_icon = new Texture("Policemann.png");
        gamepad = new Texture("gamepad.png");
        wallVertical = new Texture("wallVertical.png");
        wallHorizontal = new Texture("wallHorizontal.png");
        doorVertical = new Texture("doorVertical.png");
        doorHorizontal = new Texture("doorHorizontal.png");
        background = new Texture("FLIESEN2.PNG");
        diebFrame1 = new Texture("DiebFrame1.png");
        diebFrame2 = new Texture("DiebFrame2.png");
        diebFrame3 = new Texture("DiebFrame3.png");
        diebinFrame1 = new Texture("DiebinFrame1.png");
        diebinFrame2 = new Texture("DiebinFrame2.png");
        diebinFrame3 = new Texture("DiebinFrame3.png");
        itemIcons = new Texture[8];
        itemIcons[0] = new Texture("Item1.png");
        itemIcons[1] = new Texture("Item2.png");
        itemIcons[2] = new Texture("Item3.png");
        itemIcons[3] = new Texture("Item4.png");
        itemIcons[4] = new Texture("Item5.png");
        itemIcons[5] = new Texture("Item6.png");
        itemIcons[6] = new Texture("Item7.png");
        itemIcons[7] = new Texture("Item8.png");
        Player = new player();
        if (gameState == GameState.Ingame) setGameCamera();
        time = 0;
        GAMEPAD_SIZE = scale(GAMEPAD_SIZE, true);
        GAMEPAD_BUTTON_SIZE = scale(GAMEPAD_BUTTON_SIZE, true);

        createLevel();
        policemen = new police[level[currentLevel].policemen_count];
        resetPolice();
        items = new item[level[currentLevel].item_count];
        generateItems();
    }

    public void setGameCamera() {
        camera.position.x = Player.x;
        camera.position.y = Player.y;
        camera.update();
        batch.setProjectionMatrix(camera.combined);
    }

    public void resetPlayer() {
        Player = new player();
        setGameCamera();
    }

    public void resetPolice() {
        for (int i = 0; i < level[currentLevel].policemen_count; i++) {
            policemen[i] = new police(
                    (int) (Math.random() * (level[currentLevel].wallCountX - 1)) * level[currentLevel].wallLength + level[currentLevel].wallLength / 2,
                    (int) (Math.random() * (level[currentLevel].wallCountY - 1)) * level[currentLevel].wallLength + level[currentLevel].wallLength / 2);
            if (policemen[i].x < level[currentLevel].wallCountX && policemen[i].y < level[currentLevel].wallCountY) {
                policemen[i].x = level[currentLevel].wallLength;        //anti spawn kill
            }
        }
    }

    public void generateItems() {
        for (int i = 0; i < level[currentLevel].item_count; i++) {
            items[i] = new item(((int) (Math.random() * (level[currentLevel].wallCountX - 1))) * level[currentLevel].wallLength + level[currentLevel].wallLength / 2,
                    ((int) (Math.random() * (level[currentLevel].wallCountY - 1))) * level[currentLevel].wallLength + level[currentLevel].wallLength / 2,
                    itemIcons[(int) (Math.random() * itemIcons.length)]);
        }
    }

    public void exitButton() {
        batch.draw(new TextureRegion(zurueck), camera.position.x - Gdx.graphics.getWidth() / 2 + scale(50, true), camera.position.y + Gdx.graphics.getHeight() / 2 - scale(200, false), scale(150, true), scale(150, false));
        if (Gdx.input.getX() > scale(50, true) && Gdx.input.getX() < scale(200, false) && Gdx.input.getY() > scale(50, true) && Gdx.input.getY() < scale(200, false)) {
            camera.position.x = (Gdx.graphics.getWidth() / 2);
            camera.position.y = (Gdx.graphics.getHeight() / 2);
            camera.update();
            batch.setProjectionMatrix(camera.combined);
            if (gameState == GameState.GameOver) {            //reset game if leaving GameOverScreen
                currentLevel = (int) (Math.random() * level.length);
                resetPlayer();
                resetPolice();
                generateItems();
                camera.position.x = (Gdx.graphics.getWidth() / 2);
                camera.position.y = (Gdx.graphics.getHeight() / 2);
                camera.update();
                batch.setProjectionMatrix(camera.combined);
            }
            gameState = GameState.Menu;
        }
    }

    public void drawChar(TextureRegion icon, float x, float y, int direction_) {
        switch (direction_) {
            case 1:
                x += PLAYER_SIZE;
                break;
            case 2:
                y += PLAYER_SIZE;
                x += PLAYER_SIZE;
                break;
            case 3:
                y += PLAYER_SIZE;
        }
        batch.draw(icon, x - PLAYER_SIZE / 2, y - PLAYER_SIZE / 2, 0, 0, PLAYER_SIZE, PLAYER_SIZE, 1, 1, direction_ * 90);
    }

    public void drawGame() {
        for (int i = 0; i < level[currentLevel].wallCountX - 1; i++) {
            for (int i2 = 0; i2 < level[currentLevel].wallCountY - 1; i2++) {
                batch.draw(new TextureRegion(background), i * level[currentLevel].wallLength, i2 * level[currentLevel].wallLength, level[currentLevel].wallLength, level[currentLevel].wallLength);
                switch (level[currentLevel].wallVertical[i][i2]) {
                    case 1:
                        batch.draw(wallVertical, i * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, i2 * level[currentLevel].wallLength, level[currentLevel].wallThickness, level[currentLevel].wallLength);
                        break;
                    case 2:
                        batch.draw(doorVertical, i * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, i2 * level[currentLevel].wallLength, level[currentLevel].wallThickness, level[currentLevel].wallLength);
                }
                switch (level[currentLevel].wallHorizontal[i][i2]) {
                    case 1:
                        batch.draw(wallHorizontal, i * level[currentLevel].wallLength, i2 * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, level[currentLevel].wallLength, level[currentLevel].wallThickness);
                        break;
                    case 2:
                        batch.draw(doorHorizontal, i * level[currentLevel].wallLength, i2 * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, level[currentLevel].wallLength, level[currentLevel].wallThickness);
                }
            }
            switch (level[currentLevel].wallHorizontal[i][level[currentLevel].wallCountY - 1]) {
                case 1:
                    batch.draw(wallHorizontal, i * level[currentLevel].wallLength, (level[currentLevel].wallCountY - 1) * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, level[currentLevel].wallLength, level[currentLevel].wallThickness);
                    break;
                case 2:
                    batch.draw(doorHorizontal, i * level[currentLevel].wallLength, (level[currentLevel].wallCountY - 1) * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, level[currentLevel].wallLength, level[currentLevel].wallThickness);
            }
        }
        for (int i2 = 0; i2 < level[currentLevel].wallCountY - 1; i2++) {
            switch (level[currentLevel].wallVertical[level[currentLevel].wallCountX - 1][i2]) {
                case 1:
                    batch.draw(wallVertical, (level[currentLevel].wallCountX - 1) * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, i2 * level[currentLevel].wallLength, level[currentLevel].wallThickness, level[currentLevel].wallLength);
                    break;
                case 2:
                    batch.draw(doorVertical, (level[currentLevel].wallCountX - 1) * level[currentLevel].wallLength - level[currentLevel].wallThickness / 2, i2 * level[currentLevel].wallLength, level[currentLevel].wallThickness, level[currentLevel].wallLength);
            }
        }
        for (int i = 0; i < items.length; i++) {
            if (!items[i].colected)
                batch.draw(items[i].icon, items[i].x - ITEM_SIZE / 2, items[i].y - ITEM_SIZE / 2, ITEM_SIZE, ITEM_SIZE);
        }
        for (int i = 0; i < level[currentLevel].policemen_count; i++) {
            drawChar(new TextureRegion(police_icon), policemen[i].x, policemen[i].y, policemen[i].dir);
        }
        drawChar(player_icon, Player.x, Player.y, Player.dir);
        if (leftHand) {
            batch.draw(gamepad, camera.position.x - Gdx.graphics.getWidth() / 2 + GAMEPAD_DISTANCE,
                    camera.position.y - Gdx.graphics.getHeight() / 2 + GAMEPAD_DISTANCE, GAMEPAD_SIZE, GAMEPAD_SIZE);
        } else {
            batch.draw(gamepad, camera.position.x + Gdx.graphics.getWidth() / 2 - GAMEPAD_SIZE - GAMEPAD_DISTANCE,
                    camera.position.y - Gdx.graphics.getHeight() / 2 + GAMEPAD_DISTANCE, GAMEPAD_SIZE, GAMEPAD_SIZE);
        }
        exitButton();
        font.draw(batch, "" + Player.score, camera.position.x + Gdx.graphics.getWidth() / 2 - scale(200, true),
                camera.position.y + Gdx.graphics.getHeight() / 2 - scale(50, false));
    }

    public void calculateGame() {
        float deltaTime = Gdx.graphics.getDeltaTime();
        time += deltaTime;
        walkMale = new Animation(0.25f, new TextureRegion(diebFrame1), new TextureRegion(diebFrame3));
        walkMale.setPlayMode(Animation.PlayMode.LOOP);
        walkFemale = new Animation(0.25f, new TextureRegion(diebinFrame1), new TextureRegion(diebinFrame3));
        walkFemale.setPlayMode(Animation.PlayMode.LOOP);
        int dir = input();
        if (dir > 0) {
            Player.dir = dir;
            move(Player, deltaTime);
            setGameCamera();
            for (int i = 0; i < items.length; i++) {
                if (characterCrash(Player, items[i]) && !items[i].colected) {
                    Player.score++;
                    items[i].colected = true;
                }
            }
            if (character_icon) player_icon = walkMale.getKeyFrame(time);
            else player_icon = walkFemale.getKeyFrame(time);
        } else {
            if (character_icon) player_icon = new TextureRegion(diebFrame2);
            else player_icon = new TextureRegion(diebinFrame2);
        }
        movePolice(deltaTime);
    }

    public void movePolice(float deltaTime) {
        for (int i = 0; i < level[currentLevel].policemen_count; i++) {
            /*if (policemen[i].last_turn > 0 && policemen[i].last_turn < time + policemen[i].turnSpeed){
				policemen[i].dir += (int) (Math.random() * 3) + 1;
				if(policemen[i].dir > 4)policemen[i].dir -= 4;
				policemen[i].last_turn = 0;
			}else {
				if ((int) (Math.random() * 20) == 0) {
					policemen[i].last_turn = time;
				}
				if (move(policemen[i], deltaTime)) {
					policemen[i].last_turn = time;
				}
			}*/
            if ((int) (Math.random() * 30) == 0) {
                policemen[i].dir = (int) (Math.random() * 4) + 1;
            }
            if (move(policemen[i], deltaTime)) {
                policemen[i].dir += (int) (Math.random() * 3) + 1;
                if (policemen[i].dir > 4) policemen[i].dir -= 4;
            }

            if (characterCrash(policemen[i], Player)) gameState = GameState.GameOver;
        }
    }

    public boolean characterCrash(character char1, character char2) {
        return (((char1.x < char2.x + PLAYER_SIZE) && (char1.x > char2.x - PLAYER_SIZE)) &&
                ((char1.y < char2.y + PLAYER_SIZE) && (char1.y > char2.y - PLAYER_SIZE)));
    }

    public boolean isInside(int x, int y) {
        return (Gdx.input.getX() > x && Gdx.input.getX() < x + GAMEPAD_BUTTON_SIZE &&
                Gdx.input.getY() > y && Gdx.input.getY() < y + GAMEPAD_BUTTON_SIZE);
    }

    public int input() {
        if (Gdx.input.isTouched()) {
            int addHightIdkWhy = GAMEPAD_BUTTON_SIZE;
            if (leftHand) {
                if (isInside(GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_BUTTON_SIZE - GAMEPAD_DISTANCE - addHightIdkWhy)) {
                    return 1;            //left
                }
                if (isInside(GAMEPAD_BUTTON_SIZE + GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_DISTANCE - GAMEPAD_BUTTON_SIZE * 2 - addHightIdkWhy)) {
                    return 4;            //up
                }
                if (isInside(GAMEPAD_BUTTON_SIZE * 2 + GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_BUTTON_SIZE - GAMEPAD_DISTANCE - addHightIdkWhy)) {
                    return 3;            //right
                }
                if (isInside(GAMEPAD_BUTTON_SIZE + GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_DISTANCE - addHightIdkWhy)) {
                    return 2;            //down
                }
            } else {
                if (isInside(Gdx.graphics.getWidth() - GAMEPAD_SIZE - GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_BUTTON_SIZE - GAMEPAD_DISTANCE - addHightIdkWhy)) {
                    return 1;            //left
                }
                if (isInside(Gdx.graphics.getWidth() - GAMEPAD_BUTTON_SIZE * 2 - GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_DISTANCE - GAMEPAD_BUTTON_SIZE * 2 - addHightIdkWhy)) {
                    return 4;            //up
                }
                if (isInside(Gdx.graphics.getWidth() - GAMEPAD_BUTTON_SIZE - GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_BUTTON_SIZE - GAMEPAD_DISTANCE - addHightIdkWhy)) {
                    return 3;            //right
                }
                if (isInside(Gdx.graphics.getWidth() - GAMEPAD_BUTTON_SIZE * 2 - GAMEPAD_DISTANCE,
                        Gdx.graphics.getHeight() - GAMEPAD_DISTANCE - addHightIdkWhy)) {
                    return 2;            //down
                }
            }
        }
        return 0;
    }

    public boolean move(character Character, float deltaTime) {
        boolean returnValue = false;
        float travelDistance = deltaTime * Character.ms;
        float space = freeSpace(Character);
        if (space < travelDistance) {
            travelDistance = space;
            returnValue = true;        //return true if Wall detected
        }
        if (travelDistance < 0) {            //debug
            Character.x = 80;
            Character.y = 80;
        } else            //--------test----------------
            switch (Character.dir) {
                case 1:            //left
                    Character.x -= travelDistance;
                    break;
                case 2:            //down
                    Character.y -= travelDistance;
                    break;
                case 3:            //right
                    Character.x += travelDistance;
                    break;
                case 4:            //up
                    Character.y += travelDistance;
                    break;
            }
        return returnValue;
    }

    public float freeSpace(character Character) {                //calculate distance to next wall
        if (Character.x < 0 || Character.y < 0 || Character.x > (level[currentLevel].wallCountX - 1) * level[currentLevel].wallLength
                || Character.y > (level[currentLevel].wallCountY - 1) * level[currentLevel].wallLength) {
            return -1;
        }            //--------test----------------
        float subtractions = PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2;
        int x = (int) Character.x / level[currentLevel].wallLength;
        int x2;
        int y = (int) Character.y / level[currentLevel].wallLength;
        int y2;
        float returnValue, returnValueTmp;
        switch (Character.dir) {
            case 1:            //left
                y2 = (int) (Character.y - PLAYER_SIZE / 2) / level[currentLevel].wallLength;            //wallside 2sides of character
                if (level[currentLevel].wallVertical[x][y2] == 1)
                    returnValue = Character.x - x * level[currentLevel].wallLength - subtractions;
                else returnValue = level[currentLevel].wallLength;
                y2 = (int) (Character.y + PLAYER_SIZE / 2) / level[currentLevel].wallLength;
                if (level[currentLevel].wallVertical[x][y2] == 1) {
                    returnValueTmp = Character.x - x * level[currentLevel].wallLength - subtractions;
                    if (returnValue > returnValueTmp) returnValue = returnValueTmp;
                }
                if (x > 0)
                    if ((Character.y - y * level[currentLevel].wallLength < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallHorizontal[x - 1][y] == 1) ||        //short wallside
                            ((y + 1) * level[currentLevel].wallLength - Character.y < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallHorizontal[x - 1][y + 1] == 1)) {
                        returnValueTmp = Character.x - x * level[currentLevel].wallLength - PLAYER_SIZE / 2;
                        if (returnValue > returnValueTmp) {
                            returnValue = returnValueTmp;
                        }
                    }
                return returnValue;
            case 2:            //down
                x2 = (int) (Character.x - PLAYER_SIZE / 2) / level[currentLevel].wallLength;
                if (level[currentLevel].wallHorizontal[x2][y] == 1)
                    returnValue = Character.y - y * level[currentLevel].wallLength - subtractions;
                else returnValue = level[currentLevel].wallLength;
                x2 = (int) (Character.x + PLAYER_SIZE / 2) / level[currentLevel].wallLength;
                if (level[currentLevel].wallHorizontal[x2][y] == 1) {
                    returnValueTmp = Character.y - y * level[currentLevel].wallLength - subtractions;
                    if (returnValue > returnValueTmp) returnValue = returnValueTmp;
                }
                if (y > 0)
                    if ((Character.x - x * level[currentLevel].wallLength < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallVertical[x][y - 1] == 1) ||
                            ((x + 1) * level[currentLevel].wallLength - Character.x < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallVertical[x + 1][y - 1] == 1)) {
                        returnValueTmp = Character.y - y * level[currentLevel].wallLength - PLAYER_SIZE / 2;
                        if (returnValue > returnValueTmp) {
                            returnValue = returnValueTmp;
                        }
                    }
                return returnValue;
            case 3:            //right
                y2 = (int) (Character.y - PLAYER_SIZE / 2) / level[currentLevel].wallLength;
                if (level[currentLevel].wallVertical[x + 1][y2] == 1)
                    returnValue = (x + 1) * level[currentLevel].wallLength - Character.x - subtractions;
                else returnValue = level[currentLevel].wallLength;
                y2 = (int) (Character.y + PLAYER_SIZE / 2) / level[currentLevel].wallLength;
                if (level[currentLevel].wallVertical[x + 1][y2] == 1) {
                    returnValueTmp = (x + 1) * level[currentLevel].wallLength - Character.x - subtractions;
                    if (returnValue > returnValueTmp) returnValue = returnValueTmp;
                }
                if ((Character.y - y * level[currentLevel].wallLength < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallHorizontal[x + 1][y] == 1) ||
                        ((y + 1) * level[currentLevel].wallLength - Character.y < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallHorizontal[x + 1][y + 1] == 1)) {
                    returnValueTmp = (x + 1) * level[currentLevel].wallLength - Character.x - PLAYER_SIZE / 2 - 1;//-1 = antiBug Pixel
                    if (returnValue > returnValueTmp) {
                        returnValue = returnValueTmp;
                    }
                }
                return returnValue;
            case 4:            //up
                x2 = (int) (Character.x - PLAYER_SIZE / 2) / level[currentLevel].wallLength;
                if (level[currentLevel].wallHorizontal[x2][y + 1] == 1)
                    returnValue = (y + 1) * level[currentLevel].wallLength - Character.y - subtractions;
                else returnValue = level[currentLevel].wallLength;
                x2 = (int) (Character.x + PLAYER_SIZE / 2) / level[currentLevel].wallLength;
                if (level[currentLevel].wallHorizontal[x2][y + 1] == 1) {
                    returnValueTmp = (y + 1) * level[currentLevel].wallLength - Character.y - subtractions;
                    if (returnValue > returnValueTmp) returnValue = returnValueTmp;
                }
                if ((Character.x - x * level[currentLevel].wallLength < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallVertical[x][y + 1] == 1) ||
                        ((x + 1) * level[currentLevel].wallLength - Character.x < PLAYER_SIZE / 2 + level[currentLevel].wallThickness / 2 && level[currentLevel].wallVertical[x + 1][y + 1] == 1)) {
                    returnValueTmp = (y + 1) * level[currentLevel].wallLength - Character.y - PLAYER_SIZE / 2 - 1;
                    if (returnValue > returnValueTmp) {
                        returnValue = returnValueTmp;
                    }
                }
                return returnValue;
            default:
                return 0;
        }
    }

    //-----------------------------------------------Game Over-------------------------------------------------------
    public void gameOver() {
        camera.position.x = (Gdx.graphics.getWidth() / 2);
        camera.position.y = (Gdx.graphics.getHeight() / 2);
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        music.stop();
        if (soundsOn) {
            if (!soundplay) {
                halloechen.play();
                soundplay = true;
            }
        }
        batch.draw(gameOverScreen, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        exitButton();
        if (Player.score > prefs.getInteger("Highscore", 0))
            prefs.putInteger("Highscore", Player.score);
        font.setColor(Color.RED);
        font.draw(batch, "Highscore: " + prefs.getInteger("Highscore"), scale(520, true), scale(150, false));
        prefs.flush();
        font.draw(batch, "Punkte: " + Player.score, scale(675, true), scale(325, false));
        if (Gdx.input.justTouched()) {
            if (musicOn) music.play();
            font.setColor(Color.WHITE);
            soundplay = false;
            currentLevel = (int) (Math.random() * level.length);
            resetPlayer();
            resetPolice();
            generateItems();
            setGameCamera();
            gameState = GameState.Ingame;
        }
    }

    //-----------------------------------------------Standard Functions---------------------------------------------
    @Override
    public void create() {
        batch = new SpriteBatch();
        initialiseMenu();
        initializeGame();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (gameState == GameState.Ingame) calculateGame();

        batch.begin();
        if (gameState == GameState.Settings) {
            settings();
        } else if (gameState == GameState.Menu) {
            menu();
        } else if (gameState == GameState.Ingame) {
            drawGame();
        } else if (gameState == GameState.GameOver) {
            gameOver();
        }
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        gamepad.dispose();
        police_icon.dispose();
        wallHorizontal.dispose();
        wallVertical.dispose();
        doorHorizontal.dispose();
        doorVertical.dispose();
        background.dispose();
        gameOverScreen.dispose();
        backgroundMenuOpen.dispose();
        backgroundMenuOpen.dispose();
        backgroundSettings.dispose();
        for (int i = 0; i < itemIcons.length; i++) {
            itemIcons[i].dispose();
        }
    }
}
